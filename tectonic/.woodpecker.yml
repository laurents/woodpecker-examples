# Builds a TeX file and pushes the resulting PDF file to a given repository.
#
# In the following examples, it will be assumed that you want to build
# "docs/report.tex" and push it as "archive/project.pdf" to your "pages"
# Codeberg repository.
#
# Environment variables to be set at every step (variables shared between steps
# are not supported):
# - TEX_FOLDER: the folder of the .tex file to build (e.g. "docs");
# - TEX_FILENAME: the filename (without file extension) of the .tex file to
#                 build (e.g. "report");
# - OUT_REPO_NAME: the name of the repository to push the PDF to (e.g. "pages");
# - PDF_FOLDER: the folder where the PDF will be pushed (e.g. "archive");
# - PDF_FILENAME: the filename (without file extension) of the resulting PDF
#                 document (e.g. "project").
# Trailing slashes for folders are not necessary.
#
# Secrets to set up:
# - CBTOKEN: an Access Tokens generated from your Codeberg profile;
# - CBMAIL: an email address to author the commit containing the PDF file.

pipeline:
  build:
    image: dxjoke/tectonic-docker
    environment:
      - TEX_FOLDER=docs
      - TEX_FILENAME=report
    commands:
      - tectonic -r 1 $TEX_FOLDER/$TEX_FILENAME.tex
  deploy:
    image: alpine/git
    environment:
      - TEX_FOLDER=docs
      - TEX_FILENAME=report
      - OUT_REPO_NAME=pages
      - PDF_FOLDER=archive
      - PDF_FILENAME=project
    secrets: [cbtoken, cbmail]
    commands:
      # Avoid permission denied errors
      - chmod -R a+w .
      # Set up git in a working way
      - git config --global --add safe.directory /woodpecker/src/codeberg.org/$CI_REPO_OWNER/$CI_REPO_NAME/_site
      - git config --global user.email "$CBMAIL"
      - git config --global user.name "CI Builder"
      # Clone and move the target repo
      - git clone https://codeberg.org/$CI_REPO_OWNER/$OUT_REPO_NAME.git _site
      - chmod -R a+w _site
      - cd _site
      - mkdir -p $PDF_FOLDER
      - mv ../$TEX_FOLDER/$TEX_FILENAME.pdf $PDF_FOLDER/$PDF_FILENAME.pdf
      # Push
      - git remote set-url origin https://$CBTOKEN@codeberg.org/$CI_REPO_OWNER/$OUT_REPO_NAME.git
      - git add $PDF_FOLDER/$PDF_FILENAME.pdf
      - git commit -m "Woodpecker CI LaTeX Build at $( env TZ=Europe/Berlin date + "%Y-%m-%d %X %Z" )"
      - git push
    when:
      branch: main
